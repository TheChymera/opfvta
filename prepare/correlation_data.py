import json
import math
import numpy as np
from samri.report.utilities import voxels_for_comparison, rois_for_comparison

img1 = '../data/l2/alias-block_filtered_controlled/acq-EPI_tstat.nii.gz'
img2 = '../data/vta_projection_tstat.nii.gz'

correlation_data = {}

img1_voxels, img2_voxels = voxels_for_comparison(img1, img2,
	mask_path='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	resample_voxel_size=[0.225,0.45,0.225],
	)

make_strs = lambda x: list(map(str, x))

correlation_data['voxelwise'] = {
        'functional': make_strs(img1_voxels),
        'structural': make_strs(img2_voxels),
}

img1_rois, img2_rois, roi_names = rois_for_comparison(img1, img2)

assert len(img1_rois) == len(img2_rois)
keep = [
    not (math.isnan(r1) or math.isnan(r2))
    for (r1, r2) in zip(img1_rois, img2_rois)
]

img1_rois = img1_rois[keep]
img2_rois = img2_rois[keep]
roi_names = roi_names[keep]

correlation_data['regionwise'] = {
        'functional': make_strs(img1_rois),
        'structural': make_strs(img2_rois),
        'ROIs': make_strs(roi_names),
}

with open('../data/correlation_data.json', 'w') as outfile:
	json.dump(correlation_data, outfile)
